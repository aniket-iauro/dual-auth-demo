const Hapi = require('@hapi/hapi');
const Bcrypt = require('bcrypt')

const server = Hapi.server({
    port: 3002,
    host: 'localhost'
});

const init = async () => {

    await server.register([
        { plugin: require('hapi-auth-jwt2') },
        { plugin: require('@hapi/basic') }
    ]);

    const JWT = require('jsonwebtoken');

    const secret = 'aaa';

    //for jwt auth
    const people = { // our "users database"
        1: {
            id: 1,
            name: 'Aniket Shinde'
        }
    };

    //for basic auth
    const users = {
        aniket: {
            id: '1',
            username: 'aniket',
            password: '$2a$10$EKPCPu/Xol1XiZtYDjryVuPiOIcSxuGUKlFQ7zSq12cKRPKJnDJA2'  // 'aniket'
        }
    }

    const token = JWT.sign(people[1], secret); //this will generate token and will be displayed in console for running API with JWT auth
    console.log('use ' + token);

    var validate = async function (request, username, password) {

        const user = users[username]

        if (!user) {
            return { isValid: false }
        }

        const isValid = await Bcrypt.compare(password, user.password)

        return { isValid, credentials: { id: user.id, username: user.username } }

    };


    // jwt validation function
    const jwtValidation = function (decoded, request, h) {

        // checking to see if the person is valid
        if (!people[decoded.id]) {
            return { isValid: false };
        }
        else {
            return { isValid: true };
        }
    };

    server.auth.strategy('simple', 'basic', { validate });

    server.auth.strategy('jwt', 'jwt', { validate: jwtValidation, key: secret, verifyOptions: { ignoreExpiration: true } });

    server.route([
        {
            method: "GET",
            path: "/demo",
            config: {
                auth: {
                    strategies: ['simple', 'jwt']  //basic and jwt auth 
                },
                handler: function (request, h) {
                    return { success: 'successfully' };
                }
            }
        }
    ]);

    await server.start();
    return server;
};

init()
    .then(server => {
        console.log('Server running at:', server.info.uri);
    }).catch(err => {
        console.log(err);
    });

