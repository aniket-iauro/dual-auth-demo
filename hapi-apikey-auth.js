'use strict';

// Load modules

const Boom = require('boom');
const Hoek = require('hoek');


// Declare internals

const internals = {};

exports.register = function (server, options, next) {
    server.auth.scheme('api-key', internals.implementation);
    next();
};

exports.register.attributes = {
    name: "Hapi API Key Auth",
    version: "1.0.0"
};


internals.implementation = function (server, options) {

    Hoek.assert(options, 'Missing api-key auth strategy options');
    Hoek.assert(typeof options.validateFunc === 'function', 'options.validateFunc must be a valid function in api-key scheme');

    options.headerkey = options.headerkey || 'api-key';
    const settings = Hoek.clone(options);

    const scheme = {
        authenticate: function (request, reply) {

            const req = request.raw.req;
            const apiKey = req.headers[settings.headerkey];
            if (!apiKey) {
                return reply(Boom.unauthorized(settings.headerkey + " must be present in headers", settings.headerkey, settings.unauthorizedAttributes));
            }

            settings.validateFunc(request, apiKey, (err, isValid, credentials) => {

                credentials = credentials || null;

                if (err) {
                    return reply(err, null, { credentials: credentials });
                }

                if (!isValid) {
                    return reply(Boom.unauthorized('Invalid API key', settings.headerkey, settings.unauthorizedAttributes), null, { credentials: credentials });
                }

                if (!credentials ||
                    typeof credentials !== 'object') {

                    return reply(Boom.badImplementation('Bad credentials object received for ' + settings.headerkey + ' auth validation'));
                }

                // Authenticated
                return reply.continue({ credentials: credentials });
            });
        }
    };

    return scheme;
};